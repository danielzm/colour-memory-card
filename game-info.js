var GameInfo = Class.create({
  initialize: function (options) {
    this.elementId = options.elementId || 'game-info';
    this.$element = $(this.elementId);
    this.$movesTag = $('actual-moves');
    this.$pairsTag = $('actual-pairs');
    this.$players = $('players');
    this.data = {};
    this.api = options.api;
  },
  start: function () {
    this.updateMoves(0);
    this.updatePairs(0);
    this.updatePlayerList();
  },
  resetInfo: function() {
    this.start();
  },
  updatePlayerList: function () {
    var that = this;
    var url = this.api + '?print=pretty'
    new Ajax.Request(url, {
      method: 'get',
      onComplete: function (transport) {
        that.data.players = transport.responseJSON;
        that.removeActualPlayerList();
        that.writePlayerList();
      },
      onFailure: function (err) {
        console.error('Something went wrong on users request.');
        console.error(err);
      }
    });
  },
  removeActualPlayerList: function(){
    if($$('ol.players').length > 0) {
        $$('ol.players')[0].remove();
    }
    return;
  },
  writePlayerList: function () {
    var $ol = document.createElement('ol').addClassName('players');
    $H(this.data.players).map(function(player){
        return {
          name: player[1].name,
          moves: player[1].moves
        }
      })
      .sortBy(function(player){
        return player.moves;
      })
      .each(function (player) {
        var $player = document.createElement('li'),
          $moves = document.createElement('em');

        $moves.innerHTML = ' (' + player.moves + ')';
        $player.innerHTML = player.name;
        $player.appendChild($moves);
        $ol.appendChild($player);
      });

    this.$players.appendChild($ol);
    return;
  },
  newEvent: function (event, value) {
    if (event == 'new-move') {
      this.updateMoves(value);
    } else if (event == 'pair-found') {
      this.updatePairs(value);
    } else if (event == 'user-saved') {
      this.updatePlayerList();
    } else if (event == 'board-reset') {
      this.resetInfo();
    }
    return;
  },
  updateMoves: function (moves) {
    this.moves = moves;
    this.$movesTag.innerHTML = moves;
    return;
  },
  updatePairs: function (pairs) {
    this.pairs = pairs;
    this.$pairsTag.innerHTML = pairs;
    return;
  }
});
