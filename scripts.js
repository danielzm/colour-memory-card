'use strict';

var ColourMemoryGame = Class.create({
  initialize: function (options) {
    this.api = 'https://memory-colour-game.firebaseio.com/players.json';
    this.gameInfo = new GameInfo({
      game: this,
      api: this.api
    });
    this.cardBoard = new CardBoard({
      subscribers: [this.gameInfo],
      api: this.api,
      elementId: 'card-board'
    });
    this.$cardBoard = $('card-board');
  },

  startGame: function () {
    var that = this;
    $$('.controls button')[0].observe('click', function(){
      that.resetGame();
    });

    $$('.controls button').each(function($element){
      $element.observe('keypress', function(event){
        if (event.keyCode === Event.KEY_RETURN) {
          that.resetGame();
        }
      });
    });
    this.cardBoard.start();
    this.gameInfo.start();
  },
  resetGame: function(event){
    this.cardBoard.resetBoard();
  },
});

var game = new ColourMemoryGame();
game.startGame()
