'use strict';

var CardBoard = Class.create({
  initialize: function (options) {
    options = options || {}

    this.status = 'active';
    this.pairsFound = 0;
    this.actualMoves = 0;
    this.elementId = options.elementId || 'card-board';
    this.pairs = options.pairs || 8;
    this.colorKeys = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8'];
    this.subscribers = options.subscribers || [];
    this.$board = $(this.elementId);
    this.$form = $('save-player');
    this.cardsSelector = '.cards .card';
    this.api = options.api;
  },

  start: function () {
    this._insertCards();
    this._setCardsListeners();
    this._setSubmitFormListener();
    this._setBoardKeyNavigation();
    this._focusFirstActiveCard();
  },

  _focusFirstActiveCard: function(){
    var activeCards = this._getActiveCards()
    if(activeCards.length > 0) {
      activeCards[0].focus();
    }
    return;
  },

  resetBoard: function(){
    this.$board.writeAttribute('data-game-status', 'active');
    this._notifySubscribers('board-reset');
    this.actualMoves = 0;
    this.pairsFound = 0;
    this.resetCards();
    this._focusFirstActiveCard();
  },

  resetCards: function(){
    $$(this.cardsSelector).each(function($li){
      $li.writeAttribute('data-status','active');
      $li.removeClassName('flipped');
    });
    return;
  },

  setStatus: function () {
    this.status = status || this.status;
    return;
  },

  _notifySubscribers: function (event, value) {
    this.subscribers.each(function (subscriber) {
      subscriber.newEvent(event, value);
    });
    return;
  },

  _incrementActualMoves: function () {
    this.actualMoves++;
    this._notifySubscribers('new-move', this.actualMoves);
  },

  _incrementPairsFound: function () {
    this.pairsFound++;
    this._notifySubscribers('pair-found', this.pairsFound);
  },

  _insertCards: function () {
    this.$cards = document.createElement('ul');
    this.$cards.addClassName('cards');

    var that = this,
      cards = [];

    for (var index = 0; index <= (this.pairs - 1); index++) {
      var colorKey = 'c' + (index + 1),
      cardIndex1 = index + 1,
      cardIndex2 = cardIndex1 + this.pairs;

      cards.push(this._createCard(colorKey, cardIndex1));
      cards.push(this._createCard(colorKey, cardIndex2));
    };

    $A(cards.shuffle()).each(function(card){
      that.$cards.appendChild(card);
    });

    this.$board.appendChild(this.$cards);
    return;
  },

  _createCard: function (colorKey, id) {
    var $card = document.createElement('li');
    $card.addClassName('card simple-border')
      .writeAttribute('id', 'card' + id)
      .writeAttribute('tabindex', 1)
      .writeAttribute('data-status', 'active')
      .writeAttribute('data-color', colorKey);
    return $card;
  },

  _finishGame: function () {
    this.$board.writeAttribute('data-game-status', 'over');
    this._notifySubscribers('game-over');
    this.$form.enable();
    return;
  },

  _getFlippedCards: function () {
    return $$('#' + this.elementId + ' .card.flipped') || [];
  },

  _validatePair: function () {
    var selectedCards = this._getFlippedCards();
    if (selectedCards[0].getAttribute('data-color') === selectedCards[1].getAttribute('data-color')) {
      return 'correct';
    }
    return 'wrong';
  },

  _handlePairFound: function () {
    this._incrementPairsFound();
    var selectedCards = this._getFlippedCards();

    selectedCards.each(function ($card) {
      $card.removeClassName('flipped')
        .writeAttribute('data-status', 'pair-found');
    });

    if (this.pairsFound === this.pairs) {
      this._finishGame();
    }
    return;
  },

  _resetSelectedCards: function () {
    var selectedCards = this._getFlippedCards();
    selectedCards.each(function ($card) {
      $card.removeClassName('flipped');
    });
    return;
  },

  _handleSelectedCard: function ($card, event) {
    if ($card.hasClassName('flipped') || this._getFlippedCards().length === 2) {
      return;
    } else {
      $card.addClassName('flipped');
    }

    var selectedCards = this._getFlippedCards();
    if (selectedCards.length > 1) {
      this._incrementActualMoves();

      if (this._validatePair() === 'correct') {
        setTimeout((function () {
          this._handlePairFound();
          this._focusFirstActiveCard();
        }.bind(this)), 500, this);
      }

      setTimeout((function () {
        this._resetSelectedCards();
      }.bind(this)), 500, this);

    }
    return;
  },

  _setCardsListeners: function () {
    $$(this.cardsSelector).each(function ($card) {
      var cardBoard = this;
      $card.on('click', function (event) {
        cardBoard._handleSelectedCard(event.target, event);
      });
    }, this);
  },

  _setSubmitFormListener: function () {
    var that = this;
    this.$form.observe('submit', function (event) {
      event.preventDefault();
      this.disable();
      var name = $('player-name').getValue(),
        email = $('player-email').getValue();
      that._submitPlayerScore(name, email);
    });
  },

  _submitPlayerScore: function (name, email) {
    var that = this,
      data = {
        name: name,
        email: email,
        moves: that.actualMoves
      };
    new Ajax.Request(this.api, {
      method: 'post',
      parameters: JSON.stringify(data),
      onFailure: function (err) {
        console.log(err);
      },
      onComplete: function (res) {
        if (res && res.responseJSON && res.responseJSON.name) {
          that._getPlayerPosition(res.responseJSON.name);
          that._notifySubscribers('user-saved');
        }
      }
    });
    return;
  },

  _getPlayerPosition: function (id) {
    var that = this,
      url = this.api + '?orderBy="moves"';
    new Ajax.Request(url, {
      method: 'get',
      onSuccess: function (res) {
        if (res.responseJSON) {
          // my database service is not sorting or returning the actual position
          // so I do it with prototype because of time
          var sortedPlayers = $H(res.responseJSON)
            .sortBy(function(player){ return player[1].moves; })
            .map(function(player) {return player.key; });

          that._notifyPlayerPosition(sortedPlayers.indexOf(id) + 1);
        }
      },
      onFailure: function (err) {
        console.log(err);
      }
    });

  },
  _notifyPlayerPosition: function (position) {
    alert('Your global position is: ' + position);
  },

  _setBoardKeyNavigation: function(){
    var that = this;
    this.$board.onkeydown = function(event){
      var navigationKeyCodes = {}
      navigationKeyCodes[Event.KEY_UP] = 'Up';
      navigationKeyCodes[Event.KEY_DOWN] = 'Down';
      navigationKeyCodes[Event.KEY_LEFT] = 'Left';
      navigationKeyCodes[Event.KEY_RIGHT] = 'Right';

      if(navigationKeyCodes[event.keyCode]) {
         that.navigate(event.target, navigationKeyCodes[event.keyCode]);
      } else if(event.keyCode === Event.KEY_RETURN) {
          that._handleSelectedCard(event.target, event);
      }
    };
  },

  _getActiveCards: function(){
    return $$(this.cardsSelector+'[data-status="active"]');
  },

  navigate: function(card, direction){
    var activeCards = this._getActiveCards(),
      actualIndex = activeCards.indexOf(card),
      nextIndex;
    if(direction === 'Up') {
      nextIndex = actualIndex - 4;
    } else if(direction === 'Down') {
      nextIndex = actualIndex + 4;
    } else if(direction === 'Left') {
      nextIndex = actualIndex - 1;
      if(actualIndex === 0) {
        nextIndex = (activeCards.length - 1);
      }
    } else if(direction === 'Right') {
      nextIndex = actualIndex + 1;
      if(actualIndex === (activeCards.length - 1)) {
        nextIndex = 0;
      }
    }

    if(activeCards[nextIndex]) {
      activeCards[nextIndex].focus();
    }
    return;
  },
});
